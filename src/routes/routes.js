import auth from "../modules/auth/routes";
import dashboard from "../modules/dashboard/routes";

export default [...auth, ...dashboard];
