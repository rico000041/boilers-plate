import { all } from "redux-saga/effects";
import authSagas from "../modules/auth/saga";
import profileSagas from "../modules/profile/saga";

export default function* rootSaga() {
  yield all([...authSagas, ...profileSagas]);
}
