import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

import auth from "../modules/auth/reducer";
import profile from "../modules/profile/reducer";

const authPersistConfig = {
  key: "auth",
  storage
};

export default combineReducers({
  auth: persistReducer(authPersistConfig, auth),
  profile
});
