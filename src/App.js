import React, { Component } from "react";
import PropTypes from "prop-types";
// import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import LoadingComponent from "./commons/loader";
import { Layout, Menu, Icon, Row, Col, Modal } from "antd";
import { purgeStoredState } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { deleteToken } from "./utils/token";

import "./App.css";

const { Content, Footer, Header } = Layout;

class App extends Component {
  state = { logoutVisible: false };

  static propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    // user: PropTypes.object.isRequired,
    children: PropTypes.node.isRequired
  };

  onCollapse = collapsed => {
    this.setState({ collapsed });
  };

  showModal = () => {
    this.setState({
      logoutVisible: true
    });
  };

  onOk = () => {
    const persistConfig = {
      key: "root",
      storage
    };

    const authPersistConfig = {
      key: "auth",
      storage
    };

    deleteToken();
    purgeStoredState(persistConfig);
    purgeStoredState(authPersistConfig);
    window.location.href = "/";
  };

  hideModal = () => {
    this.setState({
      logoutVisible: false
    });
  };

  render() {
    const { isAuthenticated, isLoading } = this.props;

    if (isAuthenticated) {
      if (isLoading) {
        return <LoadingComponent isLoading={isLoading} />;
      }

      return (
        <Layout style={{ minHeight: "100vh" }} className="dashboard">
          <Header>
            <div className="logo" />
            <Menu
              theme="dark"
              mode="vertical"
              defaultSelectedKeys={["/dashboard"]}
              selectedKeys={[window.location.pathname]}
              style={{ lineHeight: "64px" }}
            >
              <Menu.Item key="/dashboard">
                <NavLink to="/dashboard" className="nav-text">
                  <Icon type="dashboard" />
                  <span>Dashboard</span>
                </NavLink>
              </Menu.Item>

              <Menu.Item key="4" onClick={this.showModal}>
                <div className="menu-logout">
                  <Icon type="logout" />
                  <span>Signout</span>
                </div>
              </Menu.Item>
            </Menu>
          </Header>
          <Layout>
            <Content>{this.props.children}</Content>
            <Footer>
              <Row>
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 24 }}
                  lg={{ span: 12 }}
                >
                  Copyright 2018 © Alleo Indong. All rights reserved.
                </Col>
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 24 }}
                  lg={{ span: 11 }}
                  className="footer-selection"
                />
              </Row>
            </Footer>
          </Layout>
          <Modal
            title="Signout"
            visible={this.state.logoutVisible}
            onOk={this.onOk}
            onCancel={this.hideModal}
            okText="Ok"
            cancelText="Cancel"
          >
            <p>Are you sure you want to signout?</p>
          </Modal>
        </Layout>
      );
    }

    return (
      <div className="app">
        <main className="main">
          <Layout id="auth">
            <Content>
              <div>{this.props.children}</div>
            </Content>
            <Footer>
              <Row>
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 24 }}
                  lg={{ span: 12 }}
                >
                  <p style={{ margin: "10px 0 6px" }}>
                    Copyright 2018 © Alleo Indong. All rights reserved.
                  </p>
                </Col>
                <Col
                  xs={{ span: 24 }}
                  sm={{ span: 24 }}
                  md={{ span: 24 }}
                  lg={{ span: 11 }}
                  className="footer-selection"
                />
              </Row>
            </Footer>
          </Layout>
        </main>
      </div>
    );
  }
}

// const mapDispatchToProps = dispatch => ({});

const mapStateToProps = (state, ownState) => ({
  isAuthenticated: state.auth.isAuthenticated
});

export default connect(
  mapStateToProps
  // mapDispatchToProps
)(App);
