import request from "../../utils/api";

export const whoami = params => {
  return request("api/v1/users/me/", {
    params
  });
};
