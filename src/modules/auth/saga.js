import { call, put, takeEvery } from "redux-saga/effects";
import { actionTypes as authActionTypes } from "./reducer";
import { actionTypes as profileActionTypes } from "../profile/reducer";
import { login as loginService } from "./service";
import { whoami } from "../profile/service";

function* login(action) {
  const { payload } = action;

  try {
    const auth = yield call(loginService, payload);
    yield put({ type: authActionTypes.LOGIN_SUCCESS, auth });

    const profile = yield call(whoami);
    yield put({
      type: profileActionTypes.GET_PROFILE_REQUEST_SUCCESS,
      profile
    });
  } catch (errors) {
    yield put({ type: authActionTypes.LOGIN_FAILED, errors });
  }
}

function* watchLoginRequest() {
  yield takeEvery(authActionTypes.LOGIN_REQUEST, login);
}

export default [watchLoginRequest()];
