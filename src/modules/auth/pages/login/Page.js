import React, { Component } from "react";
import PropTypes from "prop-types";
import { Redirect } from "react-router-dom";
import { Form, Icon, Input, Button, Row, Col, Spin } from "antd";

import "./index.css";

const FormItem = Form.Item;

class Page extends Component {
  static displayName = "LoginPage";

  static propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    login: PropTypes.func.isRequired
  };

  state = {
    forgotPasswordModal: false,
    alertVerifyEmail: false
  };

  handleClose = () => {
    this.setState({
      alertVerifyEmail: false
    });
  };

  showModal = () => {
    this.setState({
      forgotPasswordModal: true
    });
  };

  handleCancel = e => {
    this.setState({
      forgotPasswordModal: false
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const credentials = {
          grant_type: "password",
          username: values.email,
          password: values.password
        };

        this.props.login(credentials);
      }
    });
  };

  render() {
    const { isAuthenticated, isLoading } = this.props;

    const { getFieldDecorator } = this.props.form;

    if (isAuthenticated) {
      return <Redirect to="/dashboard" />;
    }

    const antIcon = (
      <Icon type="loading" style={{ fontSize: "5em", color: "#D20F84" }} spin />
    );

    return (
      <div className="login-container">
        <Spin spinning={isLoading} indicator={antIcon}>
          <Row>
            <div className="logo-container">
              <h1>Buyers Portal</h1>
            </div>
            <Col
              xs={{ span: 24 }}
              sm={{ span: 24 }}
              md={{ span: 24 }}
              lg={{ span: 24 }}
              className="login-form"
            >
              <div className="auth-container">
                <h2 className="sign-in">Buyer Login</h2>
                <p>
                  This is an example page created by leo and nina on their
                  frontend experiments
                </p>

                <Form layout="vertical" onSubmit={this.handleSubmit}>
                  <FormItem>
                    {getFieldDecorator("email", {
                      rules: [
                        {
                          type: "email"
                        },
                        {
                          required: true
                        }
                      ]
                    })(
                      <Input
                        prefix={
                          <Icon
                            type="mail"
                            style={{ color: "rgba(0,0,0,.25)" }}
                          />
                        }
                        placeholder="Email Address"
                      />
                    )}
                  </FormItem>
                  <FormItem>
                    {getFieldDecorator("password", {
                      rules: [
                        {
                          required: true
                        }
                      ]
                    })(
                      <Input
                        prefix={
                          <Icon
                            type="key"
                            style={{ color: "rgba(0,0,0,.25)" }}
                          />
                        }
                        type="password"
                        placeholder="Password"
                      />
                    )}
                  </FormItem>
                  <FormItem>
                    <Row gutter={8}>
                      <Col
                        xs={{ span: 24 }}
                        sm={{ span: 24 }}
                        md={{ span: 24 }}
                        lg={{ span: 24 }}
                      >
                        <Button
                          type="primary"
                          htmlType="submit"
                          className="login-form-button"
                        >
                          Login
                        </Button>
                      </Col>
                    </Row>
                  </FormItem>
                </Form>
              </div>
            </Col>
          </Row>
        </Spin>
      </div>
    );
  }
}

const WrappedNormalLoginForm = Form.create()(Page);

export default WrappedNormalLoginForm;
