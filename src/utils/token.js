import { purgeStoredState } from "redux-persist";

export const setToken = accessToken => {
  window.localStorage.setItem("rlbpToken", accessToken);

  return accessToken;
};

export const getToken = () => {
  return window.localStorage.getItem("rlbpToken");
};

export const deleteToken = () => {
  return window.localStorage.removeItem("rlbpToken");
};

export const backToLoginPage = () => {
  deleteToken();
  purgeStoredState();
  window.location.href = "/";
};
